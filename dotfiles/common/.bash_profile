#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc 

PATH="$PATH:/home/jenkin/bash_scripts/"; export PATH
PATH="$HOME/.node_modules/bin:$PATH";
export npm_config_prefix=~/.node_modules
export HISTFILESIZE=1200
export PATH=$PATH:~/.npm-global/bin

export WALLPAPER=/home/jenkin/annex/Pictures/Wallpapers/yesman.jpg
#export WEBDAV_USERNAME=jenkin
#export WEBDAV_PASSWORD=Spodab07
export WD=~

export PATH="$HOME/.cargo/bin:$PATH"
