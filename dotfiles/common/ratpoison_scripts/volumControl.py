import os
import sys
import re

input = int(sys.argv[1])
if input == 1:
    os.system("amixer set Master 2%+")
elif input == -1:
    os.system("amixer set Master 2%-")
elif input == 0:
    os.system("amixer set Master toggle")

tmp = os.popen("amixer").read()

values = []
onOff = None
foundState = False
lines = tmp.splitlines()
for line in lines:

    # if "Mono" in line:
    out = re.search("[0-9]*[0-9][0-9]%",line)
    if "[off]" in line and foundState == False:
        onOff = "OFF"
        foundState = True
    elif "[on]" in line and foundState == False:
        onOff = "ON"
        foundState = True
    if input == 0 and foundState == True:
        os.system('ratpoison -c "echo Sound is {}"'.format(onOff))
        quit()
    if out is None:
        continue
    else:
        values.append(out.group(0))

playback = values[:2]
capture = values[2:]

os.system("ratpoison -c 'echo L speaker: {} \nR speaker: {}\nState: {}'".format(playback[0],playback[1],onOff))

