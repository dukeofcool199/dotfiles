

if pgrep -x 'unrat' > /dev/null
then
    pkill -x 'unrat'
    ratpoison -c 'echo unrat: OFF'
else
    /usr/share/ratpoison/unrat & 
    ratpoison -c 'echo unrat: ON'
fi
