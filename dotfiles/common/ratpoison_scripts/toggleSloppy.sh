

if pgrep -x 'sloppy' > /dev/null
then
    pkill -x 'sloppy'
    ratpoison -c 'echo sloppy: OFF'
else
    /usr/share/ratpoison/sloppy & 
    ratpoison -c 'echo sloppy: ON'
fi
