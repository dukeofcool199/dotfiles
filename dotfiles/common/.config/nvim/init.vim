
" General
set number	" Show line numbers
set linebreak	" Break lines at word (requires Wrap lines)
set showbreak=+++	" Wrap-broken line prefix
set textwidth=150	" Line wrap (number of cols)
set showmatch	" Highlight matching brace
set numberwidth=5 " set the amount line numbers are shifted to the righ
set equalalways " makes split windows equal in size
syntax on	    " enables syntax highlighting
" set clipboard=unnamed " shares system clipboard

filetype plugin on
set hlsearch	" Highlight all search results
set cursorline " enables cursor line
set smartcase	" Enable smart-case search
set gdefault	" Always substitute all matches in a line
set ignorecase	" Always case-insensitive
set incsearch	" Searches for strings incrementally
set nocompatible

set autoindent	" Auto-indent new lines
set shiftwidth=4	" Number of auto-indent spaces
set smartindent	" Enable smart-indent
set smarttab	" Enable smart-tabs
set softtabstop=4	" Number of spaces per Tab
set scrolloff=10 "sets the number of lines before screen starts scrolling
set encoding=utf-8
set smc=300
set tags=./tags
set timeoutlen=1000
set ttimeoutlen=0
set updatetime=500
set relativenumber


"" Advanced
set ruler	" Show row and column ruler information

set autowriteall	" Auto-write all file changes

set undolevels=1000	" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour

"" set leader key
let mapleader=" "

"escape terminal
tnoremap <esc> <C-\><C-n>
tnoremap jk <C-\><C-n>
autocmd BufEnter term://* startinsert

" spacing for after brances when hitting enter
inoremap {<cr> {<cr>}<c-o>O
inoremap [<cr> [<cr>]<c-o>O
inoremap (<cr> (<cr>)<c-o>O




"" key remapping

inoremap jk <ESC>h " remaps escaping insert
inoremap kj <ESC>h " remaps escaping insert
onoremap p i( " paired with d , used for "delete paramaters

noremap <F9> gg=G``    " makes <F9> format document
noremap <F4> :set rnu!<cr> " <F4> toggles relative number
imap <C-c> <plug>NERDCommenterInsert

""" split window navigation
noremap <silent> <c-k> :wincmd k<cr> 
noremap <silent> <c-j> :wincmd j<cr>
noremap <silent> <c-h> :wincmd h<cr>
noremap <silent> <c-l> :wincmd l<cr>

vnoremap <C-C> "*y

""" terminal navigation

:tnoremap <C-h> <C-\><C-N><C-w>h
:tnoremap <C-j> <C-\><C-N><C-w>j
:tnoremap <C-k> <C-\><C-N><C-w>k
:tnoremap <C-l> <C-\><C-N><C-w>l
:inoremap <C-h> <C-\><C-N><C-w>h
:inoremap <C-j> <C-\><C-N><C-w>j
:inoremap <C-k> <C-\><C-N><C-w>k
:inoremap <C-l> <C-\><C-N><C-w>l
:nnoremap <C-h> <C-w>h
:nnoremap <C-j> <C-w>j
:nnoremap <C-k> <C-w>k
:nnoremap <C-l> <C-w>l

"" resize windows controls
noremap <C-w>+ :resize +5<CR>
noremap <C-w>- :resize -5<CR>
noremap <C-w>< :vertical:resize -5<CR>
noremap <C-w>> :vertical:resize +5<CR>

""" fold mapping
inoremap <F9> <C-O>za
nnoremap <F9> za
onoremap <F9> <C-C>za
vnoremap <F9> zf

augroup remember_folds
    autocmd!
    autocmd BufWinLeave * mkview
    autocmd BufWinEnter * silent! loadview
augroup END


"" leader mappings 
nnoremap <leader>s :split 
nnoremap <leader>vs :vsplit 
nnoremap <leader>ev :e $MYVIMRC<cr> " hotkey for quickly editing my vimrc
nnoremap <leader>hls :set hls!<cr> " toggles highlight search
nnoremap <leader>q :q<cr> " force quit mapping
nnoremap <leader>w :w<cr> " alternative saving
nnoremap <leader>eftp :e ~/.vim/after/ftplugin<cr> " edit filetype files 
nnoremap <leader>ff gg=G`` " formats the file
nnoremap <leader>cl :set cursorline! "toggle cursorline
nnoremap <leader>rl :set rightleft!<cr>
nnoremap <leader>ee :Explore<cr>
nnoremap <leader>es :Sexplore<cr>
nnoremap <leader>esv :Vexplore<cr>
vnoremap <leader>fo zf
nnoremap <leader>r :make run<cr>
nnoremap <leader>l @:

"noremap  <leader>t :terminal<cr>
"" abbreviations
iabbrev @@ jenkin.schibel@protonmail.com
iabbrev cright Copyright Jenkin Schibel. all rights reserved and you know it.

"" insertion bindings
" insert time stamp
nnoremap <C-D> :r !date<cr>


"" restore view settings
set viewoptions=cursor,folds,slash,unix
let g:skipview_files = ['*\.vim']


 "coc settings=======================
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

""=======VIM PLUG=============="

if empty(glob('~/.vim/autoload/plug.vim'))
      silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    endif 

call plug#begin('~/.vim/plugged') 
Plug 'alvan/vim-closetag'
Plug 'docunext/closetag.vim'
Plug 'flazz/vim-colorschemes'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
"Plug 'scrooloose/syntastic' 
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'scrooloose/nerdcommenter' 
Plug 'terryma/vim-multiple-cursors'
Plug 'vim-scripts/CursorLineCurrentWindow'
Plug 'yggdroot/indentline'
Plug 'ap/vim-css-color'
Plug 'hail2u/vim-css3-syntax'
Plug 'misterbuckley/vim-definitive'
Plug 'plasticboy/vim-markdown'
Plug 'elzr/vim-json'
Plug 'sirtaj/vim-openscad'
Plug 'aklt/plantuml-syntax'
Plug 'weirongxu/plantuml-previewer.vim'
Plug 'luochen1990/rainbow'
Plug 'yegappan/grep'
Plug 'pangloss/vim-javascript'
Plug 'morhetz/gruvbox'
Plug 'sheerun/vim-polyglot' 
Plug 'mileszs/ack.vim'
Plug 'calviken/vim-gdscript3' 
Plug 'mattn/emmet-vim'
Plug 'vimwiki/vimwiki'
Plug 'ledger/vim-ledger'
Plug 'neoclide/coc.nvim', {'branch': 'release'} 
call plug#end() 

"colorscheme Benokai 

let g:rainbow_active = 1
let g:closetag_shortcut = '>'


inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"


"colorscheme 256-jungle 
"au VimEnter * RainbowParenthesesToggle
"au Syntax * RainbowParenthesesLoadRound
"au Syntax * RainbowParenthesesLoadSquare
"au Syntax * RainbowParenthesesLoadBraces
"let g:rbpt_max = 16

