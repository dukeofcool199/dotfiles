" General
set number	" Show line numbers
set linebreak	" Break lines at word (requires Wrap lines)
set showbreak=+++	" Wrap-broken line prefix
set textwidth=150	" Line wrap (number of cols)
set showmatch	" Highlight matching brace
set numberwidth=5 " set the amount line numbers are shifted to the righ
set equalalways " makes split windows equal in size
syntax on	    " enables syntax highlighting
" set clipboard=unnamed " shares system clipboard

filetype plugin on
set hlsearch	" Highlight all search results
set cursorline " enables cursor line
set smartcase	" Enable smart-case search
set gdefault	" Always substitute all matches in a line
set ignorecase	" Always case-insensitive
set incsearch	" Searches for strings incrementally
set nocompatible

set autoindent	" Auto-indent new lines
set shiftwidth=4	" Number of auto-indent spaces
set smartindent	" Enable smart-indent
set smarttab	" Enable smart-tabs
set softtabstop=4	" Number of spaces per Tab
set scrolloff=14 "sets the number of lines before screen starts scrolling
set encoding=utf-8
set smc=300
set tags=./tags
set timeoutlen=1000
set ttimeoutlen=0

"" Advanced
set ruler	" Show row and column ruler information

set autowriteall	" Auto-write all file changes

set undolevels=1000	" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour

"" set leader key
let mapleader=" "

"" key remapping

inoremap jk <ESC> " remaps escaping insert
inoremap kj <ESC> " remaps escaping insert
onoremap p i( " paired with d , used for "delete paramaters

noremap <F9> gg=G``    " makes <F9> format document
noremap <F4> :set rnu!<cr> " <F4> toggles relative number
imap <C-c> <plug>NERDCommenterInsert

""" split window navigation
nmap <silent> <c-k> :wincmd k<cr>
nmap <silent> <c-j> :wincmd j<cr>
nmap <silent> <c-h> :wincmd h<cr>
nmap <silent> <c-l> :wincmd l<cr>

"" resize windows controls
noremap <C-w>+ :resize +5<CR>
noremap <C-w>- :resize -5<CR>
noremap <C-w>< :vertical:resize -5<CR>
noremap <C-w>> :vertical:resize +5<CR>

""" fold mapping
inoremap <F9> <C-O>za
nnoremap <F9> za
onoremap <F9> <C-C>za
vnoremap <F9> zf

augroup remember_folds
    autocmd!
    autocmd BufWinLeave * mkview
    autocmd BufWinEnter * silent! loadview
augroup END


"" leader mappings 
nnoremap <leader>s :split 
nnoremap <leader>vs :vsplit 
nnoremap <leader>ev :e $MYVIMRC<cr> " hotkey for quickly editing my vimrc
nnoremap <leader>hls :set hls!<cr> " toggles highlight search
nnoremap <leader>q :q<cr> " force quit mapping
nnoremap <leader>w :w<cr> " alternative saving
nnoremap <leader>eftp :e ~/.vim/after/ftplugin<cr> " edit filetype files 
nnoremap <leader>ff gg=G`` " formats the file
nnoremap <leader>cl :set cursorline! "toggle cursorline
nnoremap <leader>rl :set rightleft!<cr>
nnoremap <leader>ee :Explore<cr>
nnoremap <leader>es :Sexplore<cr>
nnoremap <leader>esv :Vexplore<cr>
vnoremap <leader>fo zf
nnoremap <leader>r :make run<cr>
nnoremap <leader>l @:
"" abbreviations
iabbrev @@ jenkin.schibel@protonmail.com
iabbrev cright Copyright Jenkin Schibel. all rights reserved and you know it.


"" restore view settings
set viewoptions=cursor,folds,slash,unix
let g:skipview_files = ['*\.vim']



""=======VIM PLUG=============="

if empty(glob('~/.vim/autoload/plug.vim'))
      silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    endif

call plug#begin('~/.vim/plugged') 
Plug 'tpope/vim-fugitive'
Plug 'valloric/youcompleteme'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/syntastic' 
Plug 'ervandew/supertab'
Plug 'raimondi/delimitmate'
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'scrooloose/nerdcommenter' 
Plug 'terryma/vim-multiple-cursors'
"Plug 'vim-python/python-syntax'
Plug 'vim-scripts/CursorLineCurrentWindow'
Plug 'yggdroot/indentline'
Plug 'ap/vim-css-color'
Plug 'hail2u/vim-css3-syntax'
Plug 'misterbuckley/vim-definitive'
Plug 'flazz/vim-colorschemes'
Plug 'honza/vim-snippets'
Plug 'plasticboy/vim-markdown'
Plug 'elzr/vim-json'
Plug 'sirtaj/vim-openscad'
Plug 'aklt/plantuml-syntax'
Plug 'weirongxu/plantuml-previewer.vim'
"Plug 'scrooloose/vim-slumlord'
Plug 'tyru/open-browser.vim'
Plug 'skanehira/docker.vim'
Plug 'skanehira/docker-compose.vim'
Plug 'eiiches/vim-rainbowbrackets'
Plug 'sheerun/vim-polyglot'
"Plug 'jamshedvesuna/vim-markdown-preview'
Plug 'suan/vim-instant-markdown', {'for': 'markdown'}
Plug 'yegappan/grep'
Plug 'pangloss/vim-javascript'
Plug 'morhetz/gruvbox'
call plug#end() 



colorscheme 256-jungle
